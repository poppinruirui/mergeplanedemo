﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyTrigger : MonoBehaviour {

    static Vector3 vecTempScale = new Vector3();

    public static MoneyTrigger s_Instance = null;

    public BaseScale _baseScale;
    public GameObject _goContainerJinBi;
    public GameObject _goContainerRichJinBi;

    int m_nJinBiNumToGenerate = 0;

    float m_fTimeElapse = 0f;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	


	// Update is called once per frame
	void Update () {
        GenerateJinBi();
	}

    public void TriggerMoney( Plane plane )
    {
        if ( plane.GetPlaneStatus() != Main.ePlaneStatus.running_on_airline )
        {
            return;
        }

        _baseScale.BeginScale();

        m_nJinBiNumToGenerate += 5;


        // poppin test
        RichTiaoZi tiaoZi = ResourceManager.s_Instance.NewRichTiaoZi().GetComponent<RichTiaoZi>();
        tiaoZi.transform.SetParent(MoneyTrigger.s_Instance._goContainerRichJinBi.transform);
        tiaoZi.SetPos(Vector3.zero);
        tiaoZi.Begin();


    }

    void GenerateJinBi()
    {
        if (m_nJinBiNumToGenerate <= 0f)
        {
            return;
        }

        m_fTimeElapse += Time.deltaTime;
        if (m_fTimeElapse < 0.1f)
        {
            return;
        }
        m_fTimeElapse = 0f;


        // poppin test
        TiaoZiJinBi jinBi = ResourceManager.s_Instance.NewJinBi().GetComponent<TiaoZiJinBi>();
        jinBi.transform.SetParent(MoneyTrigger.s_Instance._goContainerJinBi.transform);
        vecTempScale.x = 6f;
        vecTempScale.y = 6f;
        vecTempScale.z = 1f;
        jinBi.transform.localScale = vecTempScale;
        jinBi.Begin();

        m_nJinBiNumToGenerate--;
    }
}
