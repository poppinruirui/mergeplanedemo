﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lot : MonoBehaviour {
     
    static Vector3 vecTempPos = new Vector3();

    Plane m_Plane = null;

    public SpriteRenderer m_srBoundPlaneAvatar;
    public SpriteRenderer m_srFlyingMark;

    public GameObject m_goEffectCanMerge;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetBoundPlaneAvatarVisible( bool bVisible )
    {
        if (bVisible)
        {
            m_srBoundPlaneAvatar.sprite = m_Plane.GetAvatar();
        }
        else
        {
            m_srBoundPlaneAvatar.sprite = null;
        }
       
    }

    public void SetFlyingMark( bool bVisible )
    {
        m_srFlyingMark.gameObject.SetActive( bVisible );
    }

    public void SetPlane( Plane plane )
    {
        if (plane)
        {
            m_Plane = plane;

            m_Plane.transform.SetParent(this.transform);
            vecTempPos.x = 0;
            vecTempPos.y = 0;
            vecTempPos.z = -1;
            m_Plane.SetPos(vecTempPos);

            m_Plane.SetLot(this);
        }
        else
        {
            m_Plane = plane;
            SetBoundPlaneAvatarVisible( false );
            SetFlyingMark( false );
        }

    }

    public Plane GetPlane()
    {
        return m_Plane;
    }

    public bool IsTaken()
    {
        return GetPlane() != null;
    }

    private void OnMouseDown()
    {
        // 如果该停机位绑定有正在飞的飞机，则召回。
        if ( m_Plane != null && m_Plane.GetPlaneStatus() == Main.ePlaneStatus.running_on_airline )
        {
            m_Plane.Rotate(0);
            m_Plane.transform.SetParent(this.transform);
            m_Plane.BeginRunToLot( this );
            m_Plane.EndRunOnAirline();
            Main.s_Instance.RemovePlaneFromeAirline( m_Plane );
        }

        // 如果是宝箱，则打开
    }

    public void SetEffectCanMergeVisible( bool bVisible )
    {
        m_goEffectCanMerge.SetActive( bVisible );
    }


}
