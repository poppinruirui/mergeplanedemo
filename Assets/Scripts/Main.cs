﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{

    public static Main s_Instance = null;

    public float DISTANCE_UNIT = 3f;
    public float TURN_RADIUS = 0f;
    public Vector2 START_POS = Vector2.zero;
    public float LENGTH_OF_TURN = 0f; // 转角处的长度　
    public float PERIMETER = 0f;

    public float MERGE_OFFSET = 0.5f;
    public float MERGE_TIME = 0.2f;
    public float MERGE_WAIT_TIME = 0.2f;

    public GameObject _containerRunningPlanes;

    List<Plane> m_lstRunningPlanes = new List<Plane>();

    public float[] m_aryPosThreshold = new float[8];
    public Vector2[] m_aryStartPosOfEachSegment = new Vector2[8];
    public Vector2[] m_aryCircleCenterOfTurn = new Vector2[8];

    public float m_fMergeMoveSpeed = 0f;

    public Lot[] m_aryLots;

    public StartBelt m_StartBelt;

    public float[] m_aryRoundTimeByLevel;

    public enum ePosType
    {
        left,
        left_top,
        top,
        right_top,
        right,
        right_bottom,
        bottom,
        left_bottom
    };

    public enum ePlaneStatus
    {
        idle,
        running_on_airline,
        dragging,
        running_to_lot,
        merging
    };

    private void Awake()
    {
        s_Instance = this;

        TURN_RADIUS = DISTANCE_UNIT/* * 0.5f*/;
        START_POS = new Vector2(-DISTANCE_UNIT, 0f);
        float fTotalLengthOfTurns = 2f * Mathf.PI * TURN_RADIUS;
        PERIMETER = 8f * DISTANCE_UNIT + fTotalLengthOfTurns;
        LENGTH_OF_TURN = fTotalLengthOfTurns / 4f;

        m_aryPosThreshold[(int)ePosType.left] = 1.5f * DISTANCE_UNIT;
        m_aryPosThreshold[(int)ePosType.top] = 0.5f * DISTANCE_UNIT;
        m_aryPosThreshold[(int)ePosType.right] = -1.5f * DISTANCE_UNIT;
        m_aryPosThreshold[(int)ePosType.bottom] = -0.5f * DISTANCE_UNIT;

        m_aryStartPosOfEachSegment[(int)ePosType.left] = new Vector2( -1.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT );
        m_aryStartPosOfEachSegment[(int)ePosType.left_top] = new Vector2(-1.5f * DISTANCE_UNIT, 1.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.top] = new Vector2(-0.5f * DISTANCE_UNIT, 2.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right_top] = new Vector2(0.5f * DISTANCE_UNIT, 2.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right] = new Vector2( 1.5f * DISTANCE_UNIT, 1.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.right_bottom] = new Vector2(1.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.bottom] = new Vector2( 0.5f * DISTANCE_UNIT, -2.5f * DISTANCE_UNIT);
        m_aryStartPosOfEachSegment[(int)ePosType.left_bottom] = new Vector2(-0.5f * DISTANCE_UNIT, -2.5f * DISTANCE_UNIT);

        m_aryCircleCenterOfTurn[(int)ePosType.left_top] = new Vector2( -0.5f * DISTANCE_UNIT,  1.5f * DISTANCE_UNIT );
        m_aryCircleCenterOfTurn[(int)ePosType.right_top] = new Vector2(0.5f * DISTANCE_UNIT, 1.5f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.right_bottom] = new Vector2(0.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT);
        m_aryCircleCenterOfTurn[(int)ePosType.left_bottom] = new Vector2(-0.5f * DISTANCE_UNIT, -1.5f * DISTANCE_UNIT);

        m_fMergeMoveSpeed = MERGE_OFFSET / MERGE_TIME * Time.fixedDeltaTime;


        m_aryRoundTimeByLevel = new float[32];
        for (int i = 0; i < m_aryRoundTimeByLevel.Length; i++ )
        {
            m_aryRoundTimeByLevel[i] = 7f - 0.2f * i;
        }
    }

    // Use this for initialization
    void Start()
    {
        /*
        Plane plane = ResourceManager.s_Instance.NewPlane();
        plane.transform.SetParent(_containerRunningPlanes.transform);
        plane.SetLevel(1);
        plane.BeginRun();
        */
    }

    // Update is called once per frame
    void Update()
    {



    }

    private void FixedUpdate()
    {
      
    }

   

    // 根据飞机等级，获取该飞机运行轨道一周所需的时间
    public float GetOneRoundTimeByLevel( int nLevel )
    {
        return m_aryRoundTimeByLevel[nLevel];
    }


    public void OnClickButton_BuyPlane()
    {
        

        Lot lot = null;

        // 先判断有没有空位
        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            if ( !m_aryLots[i].IsTaken() )
            {
                lot = m_aryLots[i];
                break;
            }
        }

        if ( lot == null )
        {
            UIMsgBox.s_Instance.ShowMsg("停机坪没有空位了"); 
            return;
        }

        Plane plane = ResourceManager.s_Instance.NewPlane();
        plane.SetLevel(1);
        lot.SetPlane( plane );

    }

    public void BeginRun( Plane plane )
    {
        plane.transform.SetParent(_containerRunningPlanes.transform);

        m_lstRunningPlanes.Add( plane );
        m_StartBelt.SetRunningPlanesNum( m_lstRunningPlanes.Count );
                          
    }

    public void RemovePlaneFromeAirline( Plane plane )
    {
        m_lstRunningPlanes.Remove( plane );

        m_StartBelt.SetRunningPlanesNum(m_lstRunningPlanes.Count);
    }

    public bool CheckIfCanRun()
    {
        return m_StartBelt.CheckIfCanRun();
    }

    public void ShowCanMergeLots( bool bVisible, Plane planeDragging = null )
    {
        if ( !bVisible )
        {
            for (int i = 0; i < m_aryLots.Length; i++)
            {
                Lot lot = m_aryLots[i];
                lot.SetEffectCanMergeVisible(false);

            }
        }
        else
        {
            for (int i = 0; i < m_aryLots.Length; i++ )
            {
                Lot lot = m_aryLots[i];
                if ( lot.IsTaken() )
                {
                    Plane plane = lot.GetPlane();
                    if ( plane != planeDragging && plane.GetLevel() == planeDragging.GetLevel() && plane.GetPlaneStatus() == ePlaneStatus.idle )
                    {
                        lot.SetEffectCanMergeVisible( true );
                    }
                }
            }
        }
    }

    public Lot GetOneAvailableLotToGenerateBox()
    {
        Lot lot = null;

        for (int i = 0; i < m_aryLots.Length; i++ )
        {
            
        }

        return lot;
    }


} // end class

