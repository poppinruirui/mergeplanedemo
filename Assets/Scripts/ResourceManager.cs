﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceManager : MonoBehaviour
{
    public static ResourceManager s_Instance = null;

    public GameObject m_goRecycledPlanes;

    public Sprite[] m_aryPlaneSprites;

    public GameObject m_prePlane; // prefab of plane


    public GameObject m_preJinBi;
    public GameObject m_preRichTiaoZi;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public Sprite GetPlaneSpriteByLevel( int nLevel )
    {
        return m_aryPlaneSprites[nLevel - 1];
    }

    List<Plane> m_lstRecycledPlanes = new List<Plane>();
    public Plane NewPlane()
    {
        Plane plane = null;

        if ( m_lstRecycledPlanes.Count > 0 )
        {
            plane = m_lstRecycledPlanes[0];
            plane.gameObject.SetActive( true );
            plane.Reset();
            m_lstRecycledPlanes.RemoveAt(0);
        }
        else
        {
            plane = GameObject.Instantiate(m_prePlane).GetComponent<Plane>();
        }

        return plane;
    }

    public void DeletePlane( Plane plane )
    {
        plane.gameObject.SetActive( false );
        m_lstRecycledPlanes.Add( plane );
        plane.transform.SetParent( m_goRecycledPlanes.transform );
    }

    List<GameObject> m_lstRecycledJinBi = new List<GameObject>();
    public GameObject NewJinBi()
    {
        GameObject goJinBi = null;
        if ( m_lstRecycledJinBi.Count > 0 )
        {
            goJinBi = m_lstRecycledJinBi[0];
            m_lstRecycledJinBi.RemoveAt(0);
            goJinBi.SetActive(true);
        }
        else
        {
            goJinBi = GameObject.Instantiate(m_preJinBi);
        }
        return goJinBi;
    }

    public void DeleteJinBi( GameObject goJinBi )
    {
        goJinBi.SetActive( false );
        m_lstRecycledJinBi.Add( goJinBi );
    }

    List<GameObject> m_lstRecycledRichTiaoZi = new List<GameObject>();
    public GameObject NewRichTiaoZi()
    {
        GameObject tiaozi = null;
        if ( m_lstRecycledRichTiaoZi.Count > 0 )
        {
            tiaozi = m_lstRecycledRichTiaoZi[0];
            m_lstRecycledRichTiaoZi.RemoveAt(0);
            tiaozi.SetActive( true );
        }
        else
        {
            tiaozi = GameObject.Instantiate(m_preRichTiaoZi);
        }
        return tiaozi;
    }

    public void DeleteRichTiaoZi(GameObject tiaozi)
    {
        tiaozi.SetActive( false );
        m_lstRecycledRichTiaoZi.Add( tiaozi);
    }

}
