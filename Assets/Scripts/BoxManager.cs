﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxManager : MonoBehaviour {

    public float m_fGenerateBoxInterval = 5f;

    float m_fTimeElapse = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        GenerateLoop();
	}

    void GenerateLoop()
    {
        m_fTimeElapse += Time.deltaTime;
        if ( m_fTimeElapse < m_fGenerateBoxInterval )
        {
            return;
        }
        m_fTimeElapse = 0;



    }


}
