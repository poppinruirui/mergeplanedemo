﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectManager : MonoBehaviour {

    public static EffectManager s_Instance = null;

    public enum eEffectType
    {
        merge_succeed, // 合成成功
    };

    public GameObject[] m_aryEffectPrefabs;

    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public GameObject NewEffect( eEffectType eType )
    {
        return GameObject.Instantiate( m_aryEffectPrefabs[(int)eType] );
    }





}
